<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BillingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $user = $request->user();
        $user->createOrGetStripeCustomer();
        $user = $user->asStripeCustomer();
        $trail = '';
        foreach ($user->subscriptions as $subs) {
            if($subs->status == 'trialing') {
                $trail = $subs;
            }
        }
        if ($user && !$trail) {
            return redirect('subscription');
        }
        return $next($request);
    }
}
