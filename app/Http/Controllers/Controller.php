<?php

namespace App\Http\Controllers;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function verifyEmailAddress() {
        $email = session()->get('email');
        User::where('email','=',$email)->update([
            'email_verified_at' => Carbon::now()
        ]);
        return redirect('/dashboard')->with('success','User Verified Successfully ... !');
    }
}
