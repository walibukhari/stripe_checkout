<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Inertia\Inertia;
use Stripe\Subscription;

class ManageSubscriptionController extends Controller
{
    public function __invoke(Request $request)
    {
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $user = $request->user();
        if(!isset($user->email_verified_at)) {
            $this->sendEmailToUser($request->user());
        }
        $date = Carbon::now()->addDays(1);
        $date = Carbon::parse($date);
        /* create or get customer */
        $user->createOrGetStripeCustomer();
        /* check user has a stripe customer */
        $user = $user->asStripeCustomer();
        if($user) {
            $user_subscribe = '';
            foreach ($user->subscriptions as $subs) {
                $user_subscribe = $subs;
            }
            if($user_subscribe != '') {
                $checkUserHasSubscription = $stripe->subscriptions->retrieve(
                    $user_subscribe->id,
                    []
                );
                if($checkUserHasSubscription->trial_end == Carbon::now()->timestamp) {
                    $checkout = $request->user()
                        ->newSubscription('default', config('stripe.price_id'))
                        ->checkout([
                            'success_url' => route('dashboard'),
                            'cancel_url' => route('subscription')
                        ]);
                    return Inertia::render('ManageSubscription', [
                        'stripeKey' => config('cashier.key'),
                        'checkoutSessionId' => $checkout->id,
                        'message' => 'Not_Subscribed'
                    ]);
                } else {
                    $products = $stripe->products->retrieve(
                        $checkUserHasSubscription->plan->product,
                        []
                    );
                    $message = 'Subscribed';
                    if($user_subscribe->status == 'trialing') {
                        $message = 'Trial Period';
                    }
                    return Inertia::render('Dashboard', [
                        'stripeKey' => config('cashier.key'),
                        'plans' => $checkUserHasSubscription->plan,
                        'products' => $products,
                        'trial_ended' => Carbon::parse($checkUserHasSubscription->trial_end),
                        'message' => $message
                    ]);
                }
            } else {
                $checkout = $request->user()
                    ->newSubscription('default', config('stripe.price_id'))
                    ->checkout([
                        'success_url' => route('dashboard'),
                        'cancel_url' => route('subscription')
                    ]);
                return Inertia::render('ManageSubscription', [
                    'stripeKey' => config('cashier.key'),
                    'checkoutSessionId' => $checkout->id,
                    'message' => 'Trail Period Expired'
                ]);
            }
        }
    }

    public function subscriptionTrial(Request $request){
        $stripe = new \Stripe\StripeClient(
            env('STRIPE_SECRET')
        );
        $user = $request->user();
        $date = Carbon::now()->addDays(1);
        $date = Carbon::parse($date);
        $user->createOrGetStripeCustomer();
        /* check user has a stripe customer */
        $user = $user->asStripeCustomer();
        $user_trial = '';
        foreach ($user->subscriptions as $subs) {
            $user_trial = $subs;
        }
        if($user_trial == '') {
            $subs = $stripe->subscriptions->create([
                'customer' => $user->id,
                'items' => [
                    [
                        'price' => config('stripe.price_id')
                    ],
                ],
                'trial_end' => $date->timestamp,
            ]);
            dd($subs);
            $payment_method = \Stripe\PaymentMethod::retrieve('{{PAYMENT_METHOD_ID}}');
            $payment_method->attach(['customer' => '{{CUSTOMER_ID}}']);
        }
        $products = $stripe->products->retrieve(
            $user_trial->plan->product,
            []
        );
        $endTrial = Carbon::parse($user_trial->trial_end);
        return collect([
            'stripeKey' => config('cashier.key'),
            'message' => 'Trail Period Subscribed ... !',
            'trial_end' => $endTrial,
            'plans' => $user_trial->plan,
            'products' => $products,
        ]);
    }

    public function stripeWebhook(Request $request){
        dd($request->all());
    }

    public function plans(){
        $stripe = new \Stripe\StripeClient(
            'sk_test_51IsLfHLLNE7VJKg5hxmtyd18xJ9Dq3iRWVEFl6fbKWvOs3Ogwp6lA0ydq9IYTwPAAupjYnt2YJrVXhRGHkOZ0SqF00c7DTJoFO'
        );
        $plans = $stripe->plans->all();
        foreach($plans as $plan) {
            $prod = $stripe->products->retrieve(
                $plan->product,[]
            );
            $plan->product = $prod;
        }
        return collect([
           'status' => true,
           'data' => $plans
        ]);
    }

    public function sendEmailToUser($user){
        $to_name = $user->name;
        $to_email = $user->email;
        $data = array('name'=>'Stripe Subscription', 'body' => 'A Test Stripe Subscription Email');
        session()->put('email',$user->email);
        Mail::send('mails.verification_email', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                ->subject('Stripe Subscription Test Mail');
            $message->from('taiga@softsuitetechnologies.com','Stripe Subscription Email');
        });
    }
}
