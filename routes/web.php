<?php

use App\Http\Controllers\BillingPortalController;
use App\Http\Controllers\ManageSubscriptionController;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return Inertia::render('Welcome', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('', function () {
   return redirect('/login');
});
Route::get('/verify',[Controller::class , 'verifyEmailAddress'])->name('verifyEmailAddress');

Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::middleware('billing')->get('/dashboard', function () {
        return Inertia::render('Dashboard');
    })->name('dashboard');

    Route::get('/plans',[ManageSubscriptionController::class , 'plans'])->name('plans');
    Route::get('/subscription', ManageSubscriptionController::class)->name('subscription');
    Route::get('subscription/trial', [ManageSubscriptionController::class , 'subscriptionTrial'])->name('subscriptionTrial');
    Route::get('/billing-portal', BillingPortalController::class)->name('billing-portal');
    Route::get('/stripe/webhook', [ManageSubscriptionController::class , 'stripeWebhook'])->name('stripeWebhook');
});
